SiteChecker - Drupal 9 module
=======================
- Project site: https://gitlab.com/koplin/drupal-sitechecker
- Code: https://gitlab.com/koplin/drupal-sitechecker
- Version: 0.1
- Updated: 20.07.2021
- Author: Maciej Koplin


O module
-------------
Moduł umożliwia cykliczne sprawdzenie adresów URL poprzez odpytanie adresu strony internetowej biblioteką PHP CURL.
W rezultacie zwracany jest kod odpowiedzi HTTP oraz czas odpowiedzi pierwszego bajta.
Dane przechowywane są w dedykowanej tabeli `sitechecker_status` wzbogacone o pełną odpowiedź biblioteki CURL.

**Struktura bazy danych:**
- `id` (int) (autoincrement) - Unikalny ID testu.
- `nid` (varchar) - Node ID dla strony testu.
- `response` (blob) - Surowe dane zwrócone przez bibliotkę CURL dotyczącą transferu (https://php.net/manual/en/function.curl-getinfo.php) 
- `respone_code` (varchar) - status kodu odpowiedzi HTTP (https://developer.mozilla.org/en-US/docs/Web/HTTP/Status) lub Nxx w przypadku braku odpowiedzi.
- `response_time` (float) - czas odpowiedzi pierwszego bajtu (TTFB) w sekundach (starttransfer_time).
- `created` (int) - Znacznik czasu (timestamp) uruchomienia testu.


Wymagania
-------------
- Drupal 9.x
- PHP 7.x


Wymagane modułu
-------------
- content_access https://www.drupal.org/project/content_access
- mailsystem:mailsystem https://www.drupal.org/project/mailsystem


Instalacja
-------------
1. Pobierz archiwum ze strony projektu: https://gitlab.com/koplin/drupal-sitechecker
2. Przejdź do zakładki Rozszerzeń w panelu administracyjnym (`/admin/modules`), Panel administracyjny -> Rozszerz
3. Kliknij "Dodaj nowy moduł" (Add new module) (`/admin/modules/install`)
4. Wskaż pobrane wcześniej archiwum modułu: `sitechecker.tar.gz`.
5. Dodaj moduł do swojego projektu.   
6. Wyszukaj moduł na liście dostępnych modułów (`/admin/modules`), zaznacz i zainstaluj.
7. Po instalacji przejdź do zakładki Uprawnienia (`Permisionss`) (`/admin/people/permissions#module-sitechecker`) i upewnij się, że grupa `Administer SiteChecker Settings` posiada wymagane przez Ciebie uprawnienia. Zalecane jest aby tylko administrator systemu posiadał uprawnienie konfiguracji i wyświetlania rezultatów testów. 


Konfiguracja
-----------------------
1. Po instalacji modułu automatycznie  tworzony jest nowy ContentType (`Site Configuration`) wraz z przykładową zawartością (`Node`).
2. W celu przygotowania własnego testu dodaj nową treść w widoku Zawartości, typu `Site Configuration`.
3. Uzupełnij pola
- `Title` - (SiteConfiguration) - pole wymagane, tytuł treści.
- `URL` - Adres url (Odnośnik) - pole wymagane, zewnętrzny adres url testu.
- `Frequency` (Lista) - pole wymagane, predefiniowana lista częstotliwości odpytywania adresu url poprzez cron'a. 
- `Start At` (DateTime) - pole wymagane, data i godzina rozpoczęcia testu.
4. Przejdź do zakładki konfiguracji modułu (`/admin/config/content/sitechecker`) Panel administracyjny -> Tworzenie zawartości -> SiteChecker settings. 
5. Sprawdź dostępne opcje konfiguracyjne:
- `CURL Time Out` (Integer) - domyślna wartość: 10, The maximum number of seconds to allow cURL functions to execute.
- `CURL Connect Time Out` (Integer) - domyślna wartość: 10, The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.
- `Curl Return Transfer` (Boolean) - domyślna wartość: PRAWDA, Set TRUE to return the transfer as a string of the return value of `curl_exec()` instead of outputting it directly.
- `Pagination limit (Integer)` - domyślna wartość: 20, Limit rekordów wyświetlanych w tabeli rezultatów (`/sitechecker/show_results`) lub (`/admin/config/sitechecker/results`).
6. Skonfiguruj zadanie cykliczne w tablicy cronjob serwera aplikacyjnego. Ustaw częstotliwość cron'a co 1 minutę. Więcej https://www.drupal.org/docs/administering-a-drupal-site/cron-automated-tasks/cron-automated-tasks-overview . Manualnie możesz uruchomić cron bezpośrednio z panelu administracyjnego (`/admin/config/system/cron`) Panel administracyjny -> System -> Cron poprzez naciśnięcie przycisku "Uruchom cron".   

Lista predefiniowanych częstotliwości wykonania testów `Frequency`:
- 1M - co 1 minuta,
- 1H - co 1 godzinę,
- 2H - co 2 godziny,
- 4H - co 4 godziny,
- 8H - co 8 godzin,
- 12H - co 12 godzin,
- 1D - raz na dzień,
- 2D - co 2 dni,
- 1W - raz na tydzień.
Dokładna godzina uruchomienia skryptu kalkulowana jest na podstawie zawartości testu w polu `Start At`.


Zbieranie danych
-----------------------
Rezultaty testów zbierane są w dedykowanej tabeli (`/sitechecker/show_results`) lub (`/admin/config/sitechecker/settings`). 


Usuwanie danych
-----------------------
Możliwe jest usuniecie  rekordów z bazy danych. W zakładce konfiguracyjnej (`/sitechecker/settings/clear_data`) dostępna
są opcje wyczyszczenia danych dla:
- wszystkich witryn
- wybranej witryny
- dla wybranego zakresu danych pozostawiając dane z ostatniego dnia, tygodnia, miesiąca, 3 miesięcy, pół rok, roku
Operacja nieodwracalnie usuwa dane.


Notyfikacje e-mail
-----------------------
System umożliwia ustawienie powiadomień e-mail, na zadany status odpowiedzi HTTP. Konfiguracja obywa się w panelu administracyjnym (`/admin/config/sitechecker/settings/notifications`).
- Domyślnie, notyfikacje są wyłączone. Można je włączyć zaznaczając checkbox `Enabled notifications`.
- Użytkownik ma do wyboru notyfikacje na następujące odpowiedzi HTTP: 2xx, 3xx, 4xx, 5xx. Każdy rodzaj wystąpienia danego typu błędu (np. w przypadku konfiguracji notyfikacji typu „3xx”, błędy 301, 302) będzie powodował wysyłkę notyfikacji.
- Użytkownik ma również do wyboru notyfikacje kiedy TTBF jest mniejszy niż zadana w polu wartość. Notyfikację można włączyć zaznaczając checkbox `Enabled TTFB notifications` oraz wprowadzając wartość w (ms) w polu `Notify when TTFB response time is greater than: [ms]`.
- Na podany w konfiguracji adres e-mail zostaną wysłane powiadomienia.


Dane diagnostyczne
-----------------------
W celu weryfikacji poprawności wykonania zadań cyklicznych, moduł posiada włączone raportowanie do dziennika systemowego
Dziennik dostępny jest w Panelu administracyjnym -> Raporty -> Ostatnie wpisy dziennika (`/admin/reports/dblog`).
Wszystkie zdarzenia raportowane są do grupy `SiteChecker`. 

**Rodzaje zdarzeń:**
1. `SiteChecker START` - proces został uruchomiony poprawnie.
2. `SiteChecker Run Test for: https://example.com` - test został uruchomiony dla podanej witryny.
3. `SiteChecker Skip Test for: https://www.example.com` - test został pominięty (konfiguracja częstoliwości wykonania testu - `Frequency`).
4. `SiteChecker FINISHED` - proces został zakończony poprawnie.

**Rodzaje błędów:**
1. `Missing configurations for DataType: site_configuration` - brakuje zawartości dla typu treści `SiteConfiguration`
2. `Database error connection` - błąd połączenia z bazą danych, sprawdź pozostałe logi systemowe.


Odinstalowanie modułu
-----------------------
Moduł można odinstalować bezpośrednio przez widok zarządzania modułami, Panel administracyjny -> Rozszerz -> Odinstaluj (`/admin/modules/uninstall`) poprzez wyszukanie i znaczenie modułu `SiteChecker` i naciśnięcie przycisku "Odinstaluj".  

Ważne:
W momecie usuniecia modułu SiteChecker zostanie usunięta:
- konfiguracja położenia menu, 
- zmienne konfiguracyjne,
- table bazy dannych (`sitechecker_status`) wraz ze wszystkimi rekordami,
- zawartość typu treści `SiteConfiguration`,
- typ treści `SiteConfiuration`.


Inne informacje
-----------------------
Domyślnie wyłączono możliwość rejestracji i zamiany hasła użytkowników poprzez nadpisanie Routingu poprzez serwis `sitechecker.services.yml`
w `/src/Routing/RouteSubcriber.php` - linia 26, 27. Jeżeli chcesz włączyć możliwość rejestracji zakomentuj linię 26. Jeśli chcesz włączyć możliwość zmiany hasła zakomentuj linię 27.
