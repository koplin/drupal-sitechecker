<?php
/**
 * @file
 * SiteChecker Module hooks: hook_cron(), hook_install(), hook_uninstall(), hook_mail(), hook_mail_alter().
 */

use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\sitechecker\SiteCheckerHelper;

/**
 * Implements hook_install.
 * @throws EntityStorageException
 */
function sitechecker_install()
{
    $node = Node::create(array('type' => 'site_configuration'));
    $node->set('title', 'https://www.example.pl');
    $node->set('status', 1);
    $node->set('field_fequency', '1D');
    $node->set('field_url', 'https://www.example.pl');

    $node->save();
}

/**
 * Implements hook_uninstall().
 */
function sitechecker_uninstall()
{
    // Delete database
    $dbConnection = \Drupal::database();
    $dbConnection->schema()->dropTable('sitechecker_status');

    // Delete settings
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('curl_time_out')->save();
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('curl_connect_time_out')->save();
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('curl_return_transfer')->save();
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('pagination_limit')->save();
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('notification_enabled')->save();
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('notification_codes')->save();
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('notification_email')->save();
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('notification_ttfb_enabled')->save();
    \Drupal::configFactory()->getEditable('sitechecker.settings')->clear('notification_ttfb_time')->save();

    // Delete all nodes of given content type.
    try {
        $storageHandler = \Drupal::entityTypeManager()->getStorage('node');
    } catch (InvalidPluginDefinitionException $e) {
        Drupal::logger('SiteChecker')->warning('Entity does not exist - skipping');
    } catch (PluginNotFoundException $e) {
        Drupal::logger('SiteChecker')->warning('Plugin does not exist - skipping');
    }
    $nodes = $storageHandler->loadByProperties(['type' => 'site_configuration']);
    $storageHandler->delete($nodes);

    // Delete content type.
    try {
        $contentType = \Drupal::entityTypeManager()->getStorage('node_type')->load('site_configuration');
        $contentType->delete();
    } catch (Exception $exception) {
        \Drupal::logger('SiteChecker')->error('Table sitechecker_status does not exist');
    }
}

/**
 * Implements hook_theme().
 */
function sitechecker_theme(): array
{
    return [
        'sitechecker_description' => [
            'template' => 'description',
            'variables' => [],
        ],
    ];
}

/**
 * Implements hook_cron().
 *
 * We implement hook_cron() to do "background" processing. It gets called every
 * time the Drupal cron runs. We then decide what has to happen in response.
 */
function sitechecker_cron()
{
    /** @var SiteCheckerHelper $siteCheckerHelper */
    $siteCheckerHelper = \Drupal::service('sitechecker.helper');

    \Drupal::logger('SiteChecker')->info('SiteChecker START.');
    $cronConfig = \Drupal::config('sitechecker.settings');
    $curlTimeOut = $cronConfig->get('curl_time_out');
    $curlConnectTimeOut = $cronConfig->get('curl_connect_time_out');
    $curlReturnTransfer = $cronConfig->get('curl_return_transfer');

    $sites = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'type' => 'site_configuration',
        'status' => 1
    ]);
    if (empty($sites)) {
        \Drupal::logger('SiteChecker')->warning('Missing configurations for DataType: site_configuration.');

        return false;
    }

    foreach ($sites as $site) {
        $nid = $site->get('nid')->value;
        $url = $site->get('field_url')->getString();
        $frequency = $site->get('field_fequency')->getString();
        $startAt = $site->get('field_start_at')->getString();

        $lastRunFromDB = $siteCheckerHelper->getLastRunForPage($nid);

        if ($siteCheckerHelper->frequencyCheck($frequency, $startAt, $lastRunFromDB)) {
            \Drupal::logger('SiteChecker')->info('SiteChecker Run Test for: ' . $url);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_TIMEOUT, $curlTimeOut);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $curlConnectTimeOut);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, $curlReturnTransfer);
            curl_exec($ch);
            $result = curl_getinfo($ch);
            curl_close($ch);

            try {
                $responseHttpCode = $result['http_code'];
                if ($responseHttpCode == null) {
                    $responseHttpCode = 'Nxx';
                }
                $responseStartTransferTime = $result['starttransfer_time'];
                $db = Database::getConnection();
                $db->insert('sitechecker_status')
                    ->fields(
                        array(
                            'nid' => $nid,
                            'created' => time(),
                            'response' => var_export($result, true),
                            'response_code' => $responseHttpCode,
                            'response_time' => $responseStartTransferTime
                        )
                    )
                    ->execute();
            } catch (Exception $exception) {
                \Drupal::logger('SiteChecker')->error('Database error connection.');
            }

            //Notification part
            try {
                if ($siteCheckerHelper->isEnabledNotifications()) {
                    $httpCodes = $siteCheckerHelper->getHttpCodeForNotifications();
                    $responseCodeGroup = substr($responseHttpCode, 0, 1) . 'xx';
                    if (in_array($responseCodeGroup, $httpCodes)) {
                        $siteCheckerHelper->sendNotification($responseHttpCode, $url);
                    }

                    if ($siteCheckerHelper->isEnabledTTFBNotifications()) {
                        $timeWhenNotify = $siteCheckerHelper->getTTFBTimeForNotifications();
                        $ttfbResponseTime = (int)($responseStartTransferTime * 1000);
                        if ($ttfbResponseTime >= $timeWhenNotify) {
                            $siteCheckerHelper->sendNotification($responseHttpCode, $url, $ttfbResponseTime);
                        }
                    }
                }
            } catch (Exception $exception) {
                \Drupal::logger('SiteChecker')->error('Problem with send e-mail notification. Please check logs.');
            }
        } else {
            \Drupal::logger('SiteChecker')->info('SiteChecker Skip Test for: ' . $url);
        }
    }
    \Drupal::logger('SiteChecker')->info('SiteChecker FINISHED.');
}


/**
 * Implements sitechecker_mail().
 *
 * The hook send notification about page availability.
 * This hook defines a list of possible e-mail templates that this module can
 * send. Each e-mail is given a unique identifier, or 'key'.
 */
function sitechecker_mail($key, &$message, $params)
{
    $site_name = \Drupal::config('system.site')->get('name');
    $site_mail = \Drupal::config('system.site')->get('mail');

    $options = [
        'langcode' => $message['langcode'],
    ];

    switch ($key) {
        case 'notification_message':
            $message['headers']['Content-Type'] = 'text/html';
            $message['headers']['From'] = $site_name . '<' . $site_mail . '>';
            $message['subject'] = t('[Sitechecker][HTTP] Notification for @site', ['@site' => $params['url']],
                $options);
            $message['body'][] = t('<b>Sitechecker Notification:<br/><br/></b>', [], $options);
            $message['body'][] = t('Sitechecker sent you notification for the website: <b>@site</b> <br/>',
                ['@site' => $params['url']], $options);
            $message['body'][] = t('Response HTTP code is: <b>@code</b>', ['@code' => $params['http_code']], $options);

            break;
        case 'notification_ttfb_message':
            $message['headers']['Content-Type'] = 'text/html';
            $message['headers']['From'] = $site_name . '<' . $site_mail . '>';
            $message['subject'] = t('[Sitechecker][TTFB] Notification for @site', ['@site' => $params['url']],
                $options);
            $message['body'][] = t('<b>Sitechecker Notification:<br/><br/></b>', [], $options);
            $message['body'][] = t('Sitechecker sent you notification for the website: <b>@site</b> <br/>',
                ['@site' => $params['url']], $options);
            $message['body'][] = t('Response time for TTFB is: <b>@ttfb [ms]</b><br/>',
                ['@ttfb' => $params['ttfb_time']], $options);
            $message['body'][] = t('Response HTTP code is: <b>@code</b>', ['@code' => $params['http_code']],
                $options);
            break;
    }
}

/**
 * Implements sitechecker_mail_alter().
 *
 * hook_mail_alter() provides an interface to alter any aspect of email sent by
 * Drupal.
 *
 * Add signature to each email.
 */
function sitechecker_mail_alter(&$message)
{
    $options = [
        'langcode' => $message['langcode'],
    ];

    $signature = t("<br/><br/>\n--\nMail was sent by Sitechecker - Drupal module.", [], $options);
    $message['body'][] = $signature;
}
