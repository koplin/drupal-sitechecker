<?php
/**
 * @file
 * Contains \Drupal\sitechecker\Controller\SiteCheckerController.
 */

namespace Drupal\sitechecker\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\sitechecker\SiteCheckerHelper;

/**
 * Controller routines for sitechecker routes.
 */
class SiteCheckerController extends ControllerBase
{
    /**
     * SiteCheckerHelper service.
     *
     * @var SiteCheckerHelper
     */
    protected $siteCheckerHelper;

    /**
     * The Database Connection.
     *
     * @var Connection
     */
    protected $database;

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container): SiteCheckerController
    {
        $controller = new static(
            $container->get('database'),
            $container->get('sitechecker.helper')
        );
        $controller->setStringTranslation($container->get('string_translation'));

        return $controller;
    }

    /**
     * SiteCheckerController constructor.
     *
     * @param $database - The database connection.
     */
    public function __construct(Connection $database, SiteCheckerHelper $siteCheckerHelper)
    {
        $this->database = $database;
        $this->siteCheckerHelper = $siteCheckerHelper;
    }

    /**
     * A simple controller method to get results from sites with filter conditions
     */
    public function showResults(): array
    {
        // Get parameters from GET values
        $responseCodeFilter = \Drupal::request()->query->get('response_code');
        $urlFilter = \Drupal::request()->query->get('url');
        $dateFrom = \Drupal::request()->query->get('date_from');
        $dateTo = \Drupal::request()->query->get('date_to');

        // We are going to output the results in a table with a nice header.
        $header = [
            ['data' => $this->t('Test ID'), 'field' => 's.id', 'sort' => 'desc'],
            ['data' => $this->t('URL'), 'field' => 's.nid'],
            ['data' => $this->t('Response Code'), 'field' => 's.response_code'],
            ['data' => $this->t('Response Time [s]'), 'field' => 's.response_time'],
            ['data' => $this->t('Date of test'), 'field' => 's.created'],
        ];

        // Get settings
        $cronConfig = \Drupal::config('sitechecker.settings');
        $paginationLimit = $cronConfig->get('pagination_limit');

        // Using the TableSort Extender is what tells  the query object that we are sorting.
        $query = $this->database->select('sitechecker_status', 's')
            ->extend('Drupal\Core\Database\Query\TableSortExtender');
        $query->fields('s');

        // Creating query conditions from GET parameters
        if (!empty($responseCodeFilter) && !in_array($this->siteCheckerHelper::ALL_VALUES, $responseCodeFilter)) {
            $query->condition('response_code', $responseCodeFilter, 'IN');
        }
        if (!empty($urlFilter && !in_array($this->siteCheckerHelper::ALL_VALUES, $urlFilter))) {
            $query->condition('nid', $urlFilter, 'IN');
        }
        if (!empty($dateFrom)) {
            $query->condition('created', $dateFrom, '>=');
        }
        if (!empty($dateTo)) {
            $query->condition('created', $dateTo, '<=');
        }

        $tableSort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
        $pager = $tableSort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit($paginationLimit);
        $result = $pager->execute();

        //Build results
        $rows = [];
        foreach ($result as $row) {
            $node = Node::load($row->nid);
            if ($node instanceof Node) {
                $pageUrl = $node->get('field_url')->getString();
                $pageUrl = Link::fromTextAndUrl(t($pageUrl), Url::fromUri($pageUrl))->toString();
            } else {
                $pageUrl = $row->nid;
            }

            $rowDetails = array(
                'id' => $row->id,
                'nid' => $pageUrl,
                'response_code' => $row->response_code,
                'response_time' => round($row->response_time, 3),
                'created' => \Drupal::service('date.formatter')->format($row->created, 'short'),
            );

            $rows[] = ['data' => $rowDetails];
        }

        return [
            'description' => [
                '#theme' => 'sitechecker_description',
                '#description' => $this->t('description'),
                '#attributes' => [],
            ],
            $build['form'] = \Drupal::formBuilder()->getForm('\Drupal\sitechecker\Form\SiteCheckerFilterForm'),
            $build['tablesort_table'] = [
                '#theme' => 'table',
                '#header' => $header,
                '#rows' => $rows,
            ],
            $build['pager'] = [
                '#type' => 'pager',
            ]
        ];
    }
}
