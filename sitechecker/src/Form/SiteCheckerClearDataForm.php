<?php
/**
 * @file
 * Contains \Drupal\sitechecker\Form\SiteCheckerClearDataForm.
 */

namespace Drupal\sitechecker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sitechecker\SiteCheckerHelper;

/**
 * Defines a form to configure SiteChecker module settings for cleaning data in database.
 */
class SiteCheckerClearDataForm extends ConfigFormBase
{
    /**
     * SiteCheckerHelper service.
     *
     * @var SiteCheckerHelper
     */
    protected $siteCheckerHelper;

    /**
     * {@inheritdoc}
     */
    public function getFormID(): string
    {
        return 'sitechecker.admin_clear_data';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames(): array
    {
        return [
            'sitechecker.clear_data'
        ];
    }

    /**
     * SiteCheckerHelperConstructor constructor.
     */
    public function __construct(SiteCheckerHelper $siteCheckerHelper)
    {
        $this->siteCheckerHelper = $siteCheckerHelper;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        // Instantiates this form class.
        return new static(
        // Load the service required to construct this class.
            $container->get('sitechecker.helper')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, Request $request = null): array
    {
        $form['clear'] = array(
            '#type' => 'fieldset',
            '#collapsible' => true,
            '#collapsed' => true,
            '#title' => t('Data wipe options'),
            '#weight' => 26,
        );

        $form['clear']['range'] = array(
            '#type' => 'select',
            '#title' => $this->t('Keep data only for the:'),
            '#options' => $this->siteCheckerHelper->getWipeRanges(),
            '#key_type' => 'associative',
            '#default_value' => \Drupal::request()->query->get('response_code'),
        );

        $form['clear']['url'] = array(
            '#type' => 'select',
            '#title' => $this->t('Select page:'),
            '#options' => $this->siteCheckerHelper->getUniqueValue('nid', true),
            '#key_type' => 'associative',
            '#default_value' => \Drupal::request()->query->get('url'),
        );

        $form['clear']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Clear data'),
            '#attributes' => array('onclick' => 'if(!confirm("Are your sure?")){return false;}'),
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $messenger = \Drupal::messenger();
        $fields = $form_state->getValues();
        $nid = $fields['url'];
        $range = $fields['range'];

        if (!empty($nid) and !empty($range)) {
            $result = $this->siteCheckerHelper->clearData($nid, $range);

            if ($result >= 0) {
                $messenger->addStatus('Successfully deleted ' . $result . ' records.', $messenger::TYPE_STATUS);
            } else {
                $messenger->addWarning('Unknown error while deleting records', $messenger::TYPE_ERROR);
            }
        }
        parent::submitForm($form, $form_state);
    }
}
