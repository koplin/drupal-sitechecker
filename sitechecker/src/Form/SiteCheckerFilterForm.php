<?php
/**
 * @file
 * Contains \Drupal\sitechecker\Form\SiteCheckerFilterForm.
 */

namespace Drupal\sitechecker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\sitechecker\SiteCheckerHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines a form to configure SiteChecker module settings
 */
class SiteCheckerFilterForm extends ConfigFormBase
{
    /**
     * SiteCheckerHelper service.
     *
     * @var SiteCheckerHelper
     */
    protected $siteCheckerHelper;

    /**
     * SiteCheckerHelperConstructor constructor.
     */
    public function __construct(SiteCheckerHelper $siteCheckerHelper)
    {
        $this->siteCheckerHelper = $siteCheckerHelper;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        // Instantiates this form class.
        return new static(
        // Load the service required to construct this class.
            $container->get('sitechecker.helper')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormID(): string
    {
        return 'sitechecker.filter_form';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames(): array
    {
        return [
            'sitechecker.filter'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, Request $request = null): array
    {
        $defaultValueDateForm = null;
        $getDateFrom = \Drupal::request()->query->get('date_from');

        $form['filter'] = array(
            '#type' => 'fieldset',
            '#collapsible' => true,
            '#collapsed' => true,
            '#title' => t('Filter option'),
            '#weight' => 26,
        );

        $form['filter']['response_code'] = array(
            '#type' => 'select',
            '#title' => $this->t('Select response code:'),
            '#options' => $this->siteCheckerHelper->getUniqueValue('response_code'),
            '#key_type' => 'associative',
            '#multiple' => true,
            '#default_value' => \Drupal::request()->query->get('response_code'),
        );

        $form['filter']['url'] = array(
            '#type' => 'select',
            '#title' => $this->t('Select page:'),
            '#options' => $this->siteCheckerHelper->getUniqueValue('nid', true),
            '#key_type' => 'associative',
            '#multiple' => true,
            '#default_value' => \Drupal::request()->query->get('url'),
        );

        if (!empty($getDateFrom)) {
            $defaultValueDateForm = DrupalDateTime::createFromTimestamp($getDateFrom);
        } else {
            $getMinimalValue = $this->siteCheckerHelper->getMinimalValue('created');
            if (!empty($getMinimalValue)) {
                $defaultValueDateForm = DrupalDateTime::createFromTimestamp($this->siteCheckerHelper->getMinimalValue('created'));
            }
        }
        $form['filter']['date_from'] = array(
            '#type' => 'datetime',
            '#title' => t('Date start'),
            '#default_value' => $defaultValueDateForm,
        );

        $getDateTo = \Drupal::request()->query->get('date_to');
        $form['filter']['date_to'] = array(
            '#type' => 'datetime',
            '#title' => t('Date end'),
            '#default_value' => (!empty($getDateTo)) ? DrupalDateTime::createFromTimestamp($getDateTo) : DrupalDateTime::createFromTimestamp(time()),
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $fields = $form_state->getValues();
        $dateFrom = $fields['date_from'];
        $dateTo = $fields['date_to'];

        $parameters = array(
            'response_code' => $fields['response_code'],
            'url' => $fields['url'],
            'date_from' => (isset($dateFrom)) ? $dateFrom->getTimestamp() : null,
            'date_to' => (isset($dateTo)) ? $dateTo->getTimestamp() : null,
        );

        $currentRoutName = \Drupal::routeMatch()->getRouteName();
        $url = Url::fromRoute($currentRoutName)->setRouteParameters($parameters);

        $form_state->setRedirectUrl($url);
    }
}
