<?php
/**
 * @file
 * Contains \Drupal\sitechecker\Form\SiteCheckerNotificationsForm.
 */

namespace Drupal\sitechecker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\sitechecker\SiteCheckerHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\EmailValidator;

/**
 * Defines a form to configure SiteChecker module settings for Notification part.
 */
class SiteCheckerNotificationsForm extends ConfigFormBase
{
    /**
     * SiteCheckerHelper service.
     *
     * @var SiteCheckerHelper
     */
    protected $siteCheckerHelper;

    /**
     * @var EmailValidator
     */
    private $emailValidator;

    /**
     * {@inheritdoc}
     */
    public function getFormID(): string
    {
        return 'sitechecker.admin_notifications';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames(): array
    {
        return [
            'sitechecker.settings'
        ];
    }

    /**
     * SiteCheckerHelperConstructor constructor.
     */
    public function __construct(SiteCheckerHelper $siteCheckerHelper, EmailValidator $emailValidator)
    {
        $this->siteCheckerHelper = $siteCheckerHelper;
        $this->emailValidator = $emailValidator;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        // Instantiates this form class.
        return new static(
        // Load the service required to construct this class.
            $container->get('sitechecker.helper'),
            $container->get('email.validator')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, Request $request = null): array
    {
        $form['notifications'] = array(
            '#type' => 'fieldset',
            '#collapsible' => true,
            '#collapsed' => true,
            '#title' => t('Email notifications options'),
            '#weight' => 26,
        );

        $form['notifications']['enabled'] = array(
            '#type' => 'checkbox',
            '#title' => $this->t('Enabled notifications'),
            '#default_value' => $this->siteCheckerHelper->isEnabledNotifications(),
        );

        $form['notifications']['response_code'] = array(
            '#type' => 'select',
            '#title' => $this->t('Select response codes for notifications:'),
            '#options' => $this->siteCheckerHelper::CODES_FOR_NOTIFICATIONS,
            '#key_type' => 'associative',
            '#multiple' => true,
            '#default_value' => $this->siteCheckerHelper->getHttpCodeForNotifications(),
        );

        $form['notifications']['ttfb'] = array(
            '#type' => 'fieldset',
            '#collapsible' => true,
            '#collapsed' => true,
            '#title' => t('TTFB response time notifications:'),
            '#weight' => 10,
        );

        $form['notifications']['ttfb']['ttfb_enabled'] = array(
            '#type' => 'checkbox',
            '#title' => $this->t('Enabled TTFB notifications'),
            '#default_value' => $this->siteCheckerHelper->isEnabledTTFBNotifications(),
        );

        $form['notifications']['ttfb']['ttfb_time'] = array(
            '#type' => 'textfield',
            '#number_type' => 'decimal',
            '#title' => $this->t('Notify when TTFB response time is greater than: [ms]'),
            '#required' => true,
            '#maxlength' => 5,
            '#default_value' => $this->siteCheckerHelper->getTTFBTimeForNotifications(),
        );

        $form['notifications']['email'] = array(
            '#type' => 'email',
            '#title' => $this->t('Put email to send notifications about pages availabilities:'),
            '#required' => true,
            '#maxlength' => 50,
            '#default_value' => $this->siteCheckerHelper->getEmailForNotifications(),
            '#weight' => 100,
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $fields = $form_state->getValues();

        if (!$this->emailValidator->isValid($fields['email'])) {
            $form_state->setErrorByName('email', $this->t('That e-mail address is not valid.'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $fields = $form_state->getValues();

        // Update settings as stored in configuration.
        $this->config('sitechecker.settings')
            ->set('notification_enabled', $fields['enabled'])
            ->set('notification_codes', $fields['response_code'])
            ->set('notification_email', $fields['email'])
            ->set('notification_ttfb_enabled', $fields['ttfb_enabled'])
            ->set('notification_ttfb_time', $fields['ttfb_time'])
            ->save();

        parent::submitForm($form, $form_state);
    }
}
