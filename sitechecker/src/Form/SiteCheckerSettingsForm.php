<?php
/**
 * @file
 * Contains \Drupal\sitechecker\Form\SiteCheckerSettingsForm.
 */

namespace Drupal\sitechecker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form to configure SiteChecker module settings
 */
class SiteCheckerSettingsForm extends ConfigFormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormID(): string
    {
        return 'sitechecker.admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames(): array
    {
        return [
            'sitechecker.settings'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, Request $request = null): array
    {
        $config = $this->config('sitechecker.settings');

        $form['curl_time_out'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('CURL Time Out: [s]'),
            '#default_value' => $config->get('curl_time_out'),
            '#size' => 2,
            '#maxlength' => 3,
            '#required' => true,
            '#description' => t('The maximum number of seconds to allow cURL functions to execute.'),
        );

        $form['curl_connect_time_out'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('CURL Connect Time Out: [s]'),
            '#default_value' => $config->get('curl_connect_time_out'),
            '#size' => 2,
            '#maxlength' => 3,
            '#required' => true,
            '#description' => t('The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.'),
        );

        $form['curl_return_transfer'] = array(
            '#type' => 'checkbox',
            '#title' => $this->t('Curl Return Transfer:'),
            '#default_value' => $config->get('curl_return_transfer'),
            '#description' => t('Set TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it directly.'),
        );

        $form['pagination_limit'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Pagination limit:'),
            '#default_value' => $config->get('pagination_limit'),
            '#size' => 2,
            '#maxlength' => 3,
            '#required' => true,
            '#description' => t('Pagination limit show on the results page'),
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Update settings as stored in configuration.
        $this->config('sitechecker.settings')
            ->set('curl_time_out', $form_state->getValue('curl_time_out'))
            ->set('curl_connect_time_out', $form_state->getValue('curl_connect_time_out'))
            ->set('curl_return_transfer', $form_state->getValue('curl_return_transfer'))
            ->set('pagination_limit', $form_state->getValue('pagination_limit'))
            ->save();

        parent::submitForm($form, $form_state);
    }
}
