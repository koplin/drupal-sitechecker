<?php

/**
 * @file
 * Contains \Drupal\sitechecker\SiteCheckerHelper.
 */

namespace Drupal\sitechecker;

use DateTime;
use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Exception;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Class SiteCheckerHelper.
 *
 * @package Drupal\sitechecker
 */
class SiteCheckerHelper
{
    /**
     * Default values for condition filter - show ALL values for condition.
     */
    const ALL_VALUES = 'ALL';

    /**
     * Default HTTP codes available to send email notifications.
     */
    const CODES_FOR_NOTIFICATIONS = ['2xx' => '2xx', '3xx' => '3xx', '4xx' => '4xx', '5xx' => '5xx', 'Nxx' => 'Empty'];

    /**
     * The mail manager.
     *
     * @var \Drupal\Core\Mail\MailManagerInterface
     */
    protected $mailManager;

    /**
     * The language manager.
     *
     * @var \Drupal\Core\Language\LanguageManagerInterface
     */
    protected $languageManager;

    /**
     * Constructs a new SiteCheckerHelper.
     *
     * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
     *   The mail manager.
     * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
     *   The language manager.
     */
    public function __construct(MailManagerInterface $mailManager, LanguageManagerInterface $languageManager)
    {
        $this->mailManager = $mailManager;
        $this->languageManager = $languageManager;
    }

    /**
     * The function gets unique values from database sitechecker_status for the field provided in the $field parameter.
     *
     * @param string $field - key value
     * @param boolean $getValueByNode - if parameter is set on true, value in the result array will populate by Node name.
     *
     * @return array - array with key and value show on filter view.
     */
    public function getUniqueValue(string $field, bool $getValueByNode = false): array
    {
        $uniqueValues = array(self::ALL_VALUES => t(self::ALL_VALUES));

        $connection = Database::getConnection();
        $query = $connection->select('sitechecker_status', 's')
            ->fields('s', array($field));
        $query->groupBy("s.$field");
        $query->execute();
        $results = $query->execute()->fetchAll();

        foreach ($results as $result) {
            $value = $result->$field;
            $uniqueValues[$value] = t($value);
            if ($getValueByNode) {
                $node = Node::load($value);
                if ($node instanceof Node) {
                    $uniqueValues[$value] = $node->get('field_url')->getString();
                }
            }
        }

        return $uniqueValues;
    }

    /**
     * The function gets minimum value from database sitechecker_status for the field provided in the $field parameter.
     *
     * @param string $field - key value
     *
     * @return mixed - Return a single field from the next record, or FALSE if there isn't next record.
     */
    public function getMinimalValue(string $field)
    {
        try {
            $select = Database::getConnection()->select('sitechecker_status', 'ss');
            $select->fields('ss', array($field));
            $select->orderBy($field);
            $select->range(0, 1);
            $result = $select->execute();

            return $result->fetchField();
        } catch (Exception $exception) {
            \Drupal::logger('SiteChecker')->error('Database error connection.');

            return false;
        }
    }

    /**
     * The function checks if test for the specific page should be run.
     *
     * @param string $frequency defined frequency code
     * @param string $startAt first date when cron check should be start
     * @param string $lastRunFromDB last date when cron check was running
     *
     * @return bool
     */
    public function frequencyCheck(string $frequency, string $startAt, string $lastRunFromDB): bool
    {
        $now = new DateTime();
        $now = $now->getTimestamp();
        $lastRun = (int)$lastRunFromDB;

        if ($now >= $startAt) {
            switch ($frequency) {
                case '1M':
                    if ($now >= $lastRun + 60) {
                        return true;
                    }
                    break;
                case '1H':
                    if ($now >= $lastRun + 3600) {
                        return true;
                    }
                    break;
                case '2H':
                    if ($now >= $lastRun + 7200) {
                        return true;
                    }
                    break;
                case '4H':
                    if ($now >= $lastRun + 14400) {
                        return true;
                    }
                    break;
                case '8H':
                    if ($now >= $lastRun + 28800) {
                        return true;
                    }
                    break;
                case '12H':
                    if ($now >= $lastRun + 43200) {
                        return true;
                    }
                    break;
                case '1D':
                    if ($now >= $lastRun + 86400) {
                        return true;
                    }
                    break;
                case '2D':
                    if ($now >= $lastRun + 172800) {
                        return true;
                    }
                    break;
                case '1W':
                    if ($now >= $lastRun + 604800) {
                        return true;
                    }
                    break;
                default:
                    break;
            }
        }

        return false;
    }

    /**
     * The function checks when the cron was last run
     *
     * @param integer $nid - object identifier
     *
     * @return mixed - Return a single field from database or FALSE if there is no record in database.
     */
    public function getLastRunForPage(int $nid)
    {
        try {
            $select = Database::getConnection()->select('sitechecker_status', 'ss');
            $select->fields('ss', array('created'));
            $select->condition('nid', $nid);
            $select->orderBy('created', 'DESC');
            $select->range(0, 1);

            $result = $select->execute();

            return $result->fetchField();
        } catch (Exception $exception) {
            \Drupal::logger('SiteChecker')->error('Database error connection.');

            return false;
        }
    }

    /**
     * The function return default list of values for wipe range of data.
     *
     * @return array with default list of values
     */
    public function getWipeRanges(): array
    {
        return array(
            self::ALL_VALUES => t(self::ALL_VALUES),
            '-1 day' => t('last day'),
            '-1 week' => t("last week"),
            '-1 month' => t('last month'),
            '-3 month' => t('last 3 months'),
            '-6 month' => t('last 6 months'),
            '-1 year' => t('last year'),
        );
    }

    /**
     * The function clear data from database, based on value $nid and $range or all data.
     *
     * @param string $nid - object identifier, page
     * @param string $range - range from function getWipeRanges()
     * @return false|int - return false when is error during connect to Database, return int count of records deleted from database.
     */
    public function clearData(string $nid, string $range)
    {
        try {
            $deleteQuery = Database::getConnection()->delete('sitechecker_status');

            if ($nid and $nid != self::ALL_VALUES) {
                $deleteQuery->condition('nid', $nid);
            }

            if ($range and $range != self::ALL_VALUES) {
                $created = new DateTime();
                $created->modify($range);
                $deleteQuery->condition('created', $created->getTimestamp(), '<=');
            }

            return $deleteQuery->execute();
        } catch (Exception $exception) {
            \Drupal::logger('SiteChecker')->error('Database error connection.');

            return false;
        }
    }

    /**
     * The function return information if notification process is enabled
     *
     * @return int - 1 - enabled or 0 - disabled
     */
    public function isEnabledNotifications(): int
    {
        $config = \Drupal::config('sitechecker.settings');

        return $config->get('notification_enabled');
    }

    /**
     * The function return information if TTFB notification is enabled
     *
     * @return int - 1 - enabled or 0 - disabled
     */
    public function isEnabledTTFBNotifications(): int
    {
        $config = \Drupal::config('sitechecker.settings');

        return $config->get('notification_ttfb_enabled');
    }

    /**
     * The function return array with HTTP codes for which system should send notification.
     *
     * @return array
     */
    public function getHttpCodeForNotifications(): array
    {
        $config = \Drupal::config('sitechecker.settings');

        return $config->get('notification_codes');
    }

    /**
     * The function return int with TTFB time for which system should send notification.
     *
     * @return int
     */
    public function getTTFBTimeForNotifications(): int
    {
        $config = \Drupal::config('sitechecker.settings');

        return $config->get('notification_ttfb_time');
    }

    /**
     * The function return email for which system should send notification.
     *
     * @return string
     */
    public function getEmailForNotifications(): string
    {
        $config = \Drupal::config('sitechecker.settings');

        return $config->get('notification_email');
    }

    /**
     * This method send notification email for page availability.
     *
     * @param string $responseHttpCode - HTTP response code
     * @param string $url - Site URL address
     * @param int $ttfbResponseTime - TTFB response time
     * @return bool
     */
    public function sendNotification(string $responseHttpCode, string $url, int $ttfbResponseTime = 0): bool
    {
        $to = $this->getEmailForNotifications();
        $config = \Drupal::config('system.site');
        $from = $config->get('mail');
        $params = array(
            'http_code' => $responseHttpCode,
            'url' => $url,
            'ttfb_time' => $ttfbResponseTime
        );
        $language_code = $this->languageManager->getDefaultLanguage()->getId();


        if ($ttfbResponseTime) {
            $notificationKey = 'notification_ttfb_message';
            $loggerInfo = 'Sent e-mail notification, TTFB time: ' . $ttfbResponseTime . 'ms, HTTP status: ' . $responseHttpCode . ', page: ' . $url;
        } else {
            $notificationKey = 'notification_message';
            $loggerInfo = 'Sent e-mail notification, HTTP status: ' . $responseHttpCode . ', page: ' . $url;
        }

        $result = $this->mailManager->mail('sitechecker', $notificationKey, $to, $language_code, $params, $from,
            true);

        if ($result['result'] == true) {
            \Drupal::logger('SiteChecker')->info($loggerInfo);
        } else {
            \Drupal::logger('SiteChecker')->error('There was a problem sending your message and it was not sent.');
        }

        return $result['result'];
    }
}
